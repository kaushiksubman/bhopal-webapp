'use strict';
/**
 * @ngdoc service
 * @name WalkonWebApp.walkondialogs
 * @description
 * # walkondialogs
 * Factory in the WalkonWebApp.
 */
angular.module('WalkonWebApp')
  .factory('walkondialogsFactory', function () {
// Service logic
// ...
    var meaningOfLife = 42;
// Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
