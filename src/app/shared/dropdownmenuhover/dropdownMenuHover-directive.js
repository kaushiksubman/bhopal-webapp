'use strict';
/**
 * @ngdoc directive
 * @name WalkonWebApp.directive:dropdownMenuHover
 * @description
 * # dropdownMenuHover
 */
angular.module('WalkonWebApp')
  .directive('dropdownMenuHover', function () {
    return {
      link: function (scope, elem) {
        elem.dropdownHover();
      }
    };
  });
