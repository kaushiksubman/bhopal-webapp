'use strict';
/**
 * @ngdoc directive
 * @name WalkonWebApp.directive:a
 * @description
 * # a
 */
angular.module('WalkonWebApp')
  .directive('a', function () {
    return {
      restrict: 'E',
      link: function (scope, elem, attrs) {
        if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
          elem.on('click', function (e) {
            e.preventDefault(); // prevent link click for above criteria
          });
        }
      }
    };
  });
