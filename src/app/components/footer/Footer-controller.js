'use strict';
/**
 * @ngdoc function
 * @name WalkonWebApp.controller:FooterCtrl
 * @description
 * # FooterCtrl
 * Controller of the WalkonWebApp
 */
angular.module('WalkonWebApp')
  .controller('FooterCtrl', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
      Layout.initFooter(); // init footer
    });
  }]);
