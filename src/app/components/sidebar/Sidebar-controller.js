'use strict';
/**
 * @ngdoc function
 * @name WalkonWebApp.controller:SidebarCtrl
 * @description
 * # SidebarCtrl
 * Controller of the WalkonWebApp
 */
angular.module('WalkonWebApp')
  .controller('SidebarCtrl', ['$scope', 'User',
  	function ($scope, User ) {
  		$scope.showMenu = false;
	    $scope.$on('$includeContentLoaded', function() {
	      Layout.initSidebar(); // init sidebar
	    });
		$scope.$on('adminUserLoaded', function(event, user) { 
			console.log(user);
			if (user.entryDomainName === 'global') {
				$scope.showMenu = true;
			}
		});
  	}]);
