'use strict';
/**
 * @ngdoc function
 * @name WalkonWebApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the WalkonWebApp
 */
angular.module('WalkonWebApp')
  .controller('HeaderCtrl', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
      Layout.initHeader(); // init header
    });
  }]);
