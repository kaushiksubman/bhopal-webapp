'use strict';
/**
 * @ngdoc function
 * @name WalkonWebApp.controller:themepanelCtrl
 * @description
 * # themepanelCtrl
 * Controller of the WalkonWebApp
 */
angular.module('WalkonWebApp')
  .controller('ThemePanelCtrl', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
      Demo.init(); // init theme panel
    });
  }]);
