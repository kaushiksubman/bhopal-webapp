'use strict';
/**
 * @ngdoc function
 * @name WalkonWebApp.controller:QuickSidebarCtrl
 * @description
 * # QuickSidebarCtrl
 * Controller of the WalkonWebApp
 */
angular.module('WalkonWebApp')
  .controller('QuickSidebarCtrl', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
      setTimeout(function(){
        QuickSidebar.init(); // init quick sidebar
      }, 2000)
    });
  }]);
