'use strict';
/**
 * @ngdoc filter
 * @name WalkonWebApp.filter:components
 * @function
 * @description
 * # components
 * Filter in the WalkonWebApp.
 */
angular.module('WalkonWebApp')
  .filter('components', function () {
    return function (input) {
      return 'components filter: ' + input;
    };
  });
