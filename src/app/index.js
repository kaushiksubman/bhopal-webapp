'use strict';

(/**
 *
 * @param {Metronic} Metronic
 */
  function (Metronic) {
  var WalkonWebApp = angular.module('WalkonWebApp',
    ['WalkonWebApp.config', 'ngTouch', 'ui.router', 'ui.bootstrap',
      'ui.bootstrap.tpls', 'ngSanitize', 'ngResource', 'lbServices',
      'dialogs.main', 'dialogs.default-translations', 'ImageCropper',
      'dcbImgFallback', 'angularMoment','ui.bootstrap','ui.bootstrap.datetimepicker',
      'toggle-switch','ngCsv','summernote','ngFileUpload']);

  /********************************************
   END: BREAKING CHANGE in AngularJS v1.3.x:
   *********************************************/

  /* Setup global settings */
  WalkonWebApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages settings
    var settings = {
      layout: {
        pageSidebarClosed: false, // sidebar menu state
        pageBodySolid: false, // solid body color state
        pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
      },
      layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
      layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    }; // Image settings

    $rootScope.settings = settings;

    return settings;
  }]);


  /***
   Layout Partials.
   By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
   initialization can be disabled and Layout.init() should be called on page load complete as explained above.
   ***/

  WalkonWebApp.config(function ($stateProvider, $urlRouterProvider,
                                dialogsProvider, LoopBackResourceProvider,
                                EnvironmentConfig) {
    dialogsProvider.useBackdrop('static');
    $stateProvider
      .state('petition', {
        url: '/petition',
        templateUrl: 'petition.html',
        controller: 'PetitionCtrl'
      })
      .state('submitted', {
        url: '/submitted',
        templateUrl: 'petitionSubmitted.html',
        controller: 'SubmissionCtrl'
      });

    $urlRouterProvider.otherwise('/');
    LoopBackResourceProvider.setUrlBase(EnvironmentConfig.API_URL);

  });


  /* Init global settings and run the app */
  WalkonWebApp.run(["$rootScope", "settings", "$state", 'LoopBackAuth',
    '$stateParams', '$location', '$urlRouter', '$log',
    function ($rootScope, settings, $state, LoopBackAuth, $stateParams,
              $location, $urlRouter, $log) {
      if ($location.absUrl().indexOf('/petition.html') > -1) {
        $log.debug('login');
        return;
      }
      $rootScope.$state = $state; // state to be accessed from view
      $rootScope.$stateParams = $stateParams;
      // set sidebar closed and body solid layout mode
      $rootScope.settings.layout.pageBodySolid = true;
      $rootScope.settings.layout.pageSidebarClosed = false;
      if ($location.absUrl().indexOf('.html') < 0) {
        window.location.href = '/petition.html#/';
      }
      

    }]);


})(window.Metronic);
