'use strict';
/**
 * @ngdoc function
 * @name WalkonWebApp.controller:PetitionCtrl
 * @description
 * # PetitionCtrl
 * Controller of the WalkonWebApp
 */
angular.module('WalkonWebApp')
  .controller('PetitionCtrl', ['$scope', 'Petitioner', '$location', '$state',
    function ($scope, Petitioner, $location, $state) {
      $scope.petitioner = {};
      $scope.submitPetition = function () {
        Petitioner.create({
        	firstName: $scope.petitioner.firstName,
        	lastName: $scope.petitioner.lastName
        }, function onSuccess(newPetitioner) {
          toastr.success('Petition has been submitted successfully!');
          $scope.petitioner = newPetitioner;
          window.location.href = '/petitionSubmitted.html#/';
        }, function onError(err) {
          if (err.status === 422) {
            $scope.error =
              'Validation error. Have you signed the petition already? If not, make sure' +
              ' you have entered valid details';
          } else {
            $scope.error =
              'Petition submission failed';
          }
        });
      };
    }]);
