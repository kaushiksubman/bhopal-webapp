'use strict';
/**
 * @ngdoc function
 * @name WalkonWebApp.controller:SubmissionCtrl
 * @description
 * # SubmissionCtrl
 * Controller of the WalkonWebApp
 */
angular.module('WalkonWebApp')
  .controller('SubmissionCtrl', ['$scope', 'Petitioner', '$location', '$state', '$stateParams',
    function ($scope, Petitioner, $location, $state, $stateParams) {
      $scope.backToPetition = function () {
      	window.location.href = '/petition.html#/';
      };
    }]);
